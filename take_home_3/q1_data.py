import numpy as np
import matplotlib.pyplot as plt

from q1_parameters import *

x_list = []
label_list = []

cum_prior_list = np.cumsum(prior_list)

for _ in range(100000):

    random_num = np.random.random()

    for index, cum_prior in enumerate(cum_prior_list):

        if random_num < cum_prior:
            x = np.random.multivariate_normal(
                mean_list[index], covariance_list[index])
            x_list.append(x)
            label_list.append(index)
            break

x_list = np.array(x_list)
label_list = np.array(label_list)

np.savetxt('q1_val_x_list_100000.csv', x_list)
np.savetxt('q1_val_label_list_100000.csv', label_list, fmt='%i')

plt.hist(x_list[:, 0], 100)
plt.show()
