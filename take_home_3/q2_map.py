import numpy as np
import sklearn.model_selection
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt

from q2_parameters import *

train_x_list = np.loadtxt('q2_train_x_list.csv')
train_y_list = np.loadtxt('q2_train_y_list.csv')
val_x_list = np.loadtxt('q2_val_x_list.csv')
val_y_list = np.loadtxt('q2_val_y_list.csv')

k_fold = sklearn.model_selection.KFold(shuffle=True)

best_beta_error = np.inf
best_beta = None

for beta in np.logspace(-3, 3, num=7):

    mean_square_error_list = []

    for k_fold_train_index, k_fold_val_index in k_fold.split(train_x_list):

        k_fold_train_x_list = train_x_list[k_fold_train_index]
        k_fold_train_y_list = train_y_list[k_fold_train_index]

        est_weight = np.linalg.inv(k_fold_train_x_list.T.dot(k_fold_train_x_list)+(
            v_variance/beta)**2*np.eye(dimension+1)).dot(k_fold_train_y_list.T.dot(k_fold_train_x_list))

        k_fold_val_x_list = train_x_list[k_fold_val_index]
        k_fold_val_y_list = train_y_list[k_fold_val_index]

        mean_square_error = np.mean(
            (k_fold_val_y_list - est_weight.dot(k_fold_val_x_list.T))**2)

        mean_square_error_list.append(mean_square_error)

    # if beta == 1:
    #     print(est_weight)

    beta_error = np.mean(mean_square_error_list)

    if beta_error < best_beta_error:
        best_beta_error = beta_error
        best_beta = beta

print(best_beta)

best_est_weight = np.linalg.inv(train_x_list.T.dot(train_x_list)+(
    v_variance/best_beta)**2*np.eye(dimension+1)).dot(train_y_list.T.dot(train_x_list))

log_likelihood = len(val_y_list)*np.log(1/(2*np.pi*v_variance)**0.5) - 0.5 * \
    np.sum((val_y_list-best_est_weight.dot(val_x_list.T))**2)/v_variance

print(log_likelihood*-2)
