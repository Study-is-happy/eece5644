import numpy as np
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt

from q1_parameters import *

probability_of_error_list = []

for dataset_size in train_dataset_size_list:

    x_list = np.loadtxt('q1_train_x_list_'+str(dataset_size)+'.csv')
    label_list = np.loadtxt(
        'q1_train_label_list_'+str(dataset_size)+'.csv', dtype=int)

    likelihood_x_list = []

    for prior, mean, covariance in zip(prior_list, mean_list, covariance_list):
        likelihood_x_list.append(multivariate_normal.pdf(
            x_list, mean, covariance))

    likelihood_x_list = np.array(likelihood_x_list).T

    prior_x = np.sum(likelihood_x_list*prior_list, axis=1)

    posterior_x = likelihood_x_list * prior_list / \
        np.tile(prior_x, (num_class, 1)).T

    predict_label_list = np.argmax(posterior_x, axis=1)

    probability_of_error = np.count_nonzero(
        predict_label_list != label_list)/len(label_list)

    probability_of_error_list.append(probability_of_error)

plt.plot(train_dataset_size_list, probability_of_error_list)
plt.xticks(train_dataset_size_list)
plt.xlabel("dataset size")
plt.ylabel("probability of error")
plt.show()
