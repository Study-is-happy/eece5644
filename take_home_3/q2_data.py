import numpy as np
import matplotlib.pyplot as plt

from q2_parameters import *

dataset_dict = {'train': 100, 'val': 10000}

for dataset_name in dataset_dict:

    dataset_size = dataset_dict[dataset_name]

    x_list = np.random.multivariate_normal(
        x_mean, x_covariance, dataset_size)

    z_list = np.random.multivariate_normal(
        z_mean, z_covariance, dataset_size)

    v_list = np.random.normal(v_mean, v_variance, dataset_size)

    y_list = a.dot((x_list+z_list).T)+v_list

    np.savetxt('q2_'+dataset_name+'_x_list.csv', np.hstack(
        (np.ones((dataset_size, 1)), x_list)))
    np.savetxt('q2_'+dataset_name+'_y_list.csv', y_list)

    # plt.hist(v_list, dataset_size//10)
    # plt.show()
