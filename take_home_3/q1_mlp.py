import numpy as np
import keras
import sklearn.model_selection
import matplotlib.pyplot as plt

from q1_parameters import *

import q1_data_distribution

val_x_list = np.loadtxt('q1_val_x_list_100000.csv')
val_label_list = np.loadtxt('q1_val_label_list_100000.csv', dtype=int)

num_perceptron_list = range(5, 11)
k_fold = sklearn.model_selection.KFold(n_splits=10, shuffle=True)

best_num_perceptron_list = []
mlp_probability_of_error_list = []

for dataset_size in train_dataset_size_list:

    train_x_list = np.loadtxt('q1_train_x_list_'+str(dataset_size)+'.csv')
    train_label_list = np.loadtxt(
        'q1_train_label_list_'+str(dataset_size)+'.csv')

    best_accuracy = 0
    best_num_perceptron = None
    best_model = None

    for num_perceptron in num_perceptron_list:

        model = keras.models.Sequential()
        model.add(keras.layers.Dense(num_perceptron, kernel_initializer='random_uniform',
                                     activation='elu', input_dim=dimension))
        model.add(keras.layers.Dense(
            num_class, kernel_initializer='random_uniform', activation='softmax'))

        model.compile(loss='categorical_crossentropy')

        one_hot_train_label_list = keras.utils.to_categorical(
            train_label_list, num_classes=num_class)

        train_accuracy_list = []

        for k_fold_train_index, k_fold_val_index in k_fold.split(train_x_list):

            model.fit(train_x_list[k_fold_train_index],
                      one_hot_train_label_list[k_fold_train_index], epochs=100, verbose=0)

            predict_train_k_fold_val_label_list = np.argmax(
                model.predict(train_x_list[k_fold_val_index]), axis=1)

            train_accuracy = np.count_nonzero(
                predict_train_k_fold_val_label_list == train_label_list[k_fold_val_index])/len(k_fold_val_index)

            train_accuracy_list.append(train_accuracy)

        mean_train_accuracy = np.mean(train_accuracy_list)

        if mean_train_accuracy > best_accuracy:
            best_accuracy = mean_train_accuracy
            best_num_perceptron = num_perceptron
            best_model = model

    print('---'+str(dataset_size)+'---')

    best_num_perceptron_list.append(best_num_perceptron)
    print(best_num_perceptron)

    predict_val_label_list = np.argmax(
        best_model.predict(val_x_list), axis=1)

    val_accuracy = np.count_nonzero(
        predict_val_label_list == val_label_list)/len(val_label_list)

    mlp_probability_of_error_list.append(1-val_accuracy)
    print(1-val_accuracy)

plt.plot(train_dataset_size_list, best_num_perceptron_list)
plt.xticks(train_dataset_size_list)
plt.yticks(num_perceptron_list)
plt.xlabel("dataset size")
plt.ylabel("perceptrons")
plt.show()

plt.plot(train_dataset_size_list, mlp_probability_of_error_list)
plt.plot(train_dataset_size_list,
         q1_data_distribution.probability_of_error_list)
plt.xticks(train_dataset_size_list)
plt.xlabel("dataset size")
plt.ylabel("probability of error")
plt.show()
