import numpy as np

dimension = 3

num_class = 4

prior_list = np.array([0.1, 0.2, 0.3, 0.4])

mean_list = np.array([[-15, -15, -15],
                      [-5, -5, -5],
                      [5, 5, 5],
                      [15, 15, 15]])

covariance_list = np.array([[[7**2, 0, 0],
                             [0, 7**2, 0],
                             [0, 0, 7**2]],
                            [[7**2, 0, 0],
                             [0, 7**2, 0],
                             [0, 0, 7**2]],
                            [[7**2, 0, 0],
                             [0, 7**2, 0],
                             [0, 0, 7**2]],
                            [[7**2, 0, 0],
                             [0, 7**2, 0],
                             [0, 0, 7**2]]])

train_dataset_size_list = [100, 200, 500, 1000, 2000, 5000]
