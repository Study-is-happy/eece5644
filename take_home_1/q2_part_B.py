import numpy as np
import matplotlib.pyplot as plt
from q2_parameters import *
import util

x_list = np.loadtxt('q2_x_list.csv')
label_list = np.loadtxt('q2_label_list.csv', dtype=int)

decision_label_list = []
prev_decision_label_list = []

for x in x_list:

    condition_list = []

    for label in range(classes):
        condition_list.append(util.pdf(x, mean_list[label],
                                       covariance_list[label])*prior_list[label])

    condition_list = np.array(condition_list)

    prior_x = np.sum(condition_list*prior_list)

    posterior_x = prior_list*condition_list/prior_x

    decision_label = np.argmin(loss_matrix_B.dot(posterior_x))

    prev_decision_label = np.argmin(loss_matrix_A.dot(posterior_x))

    decision_label_list.append(decision_label)
    prev_decision_label_list.append(prev_decision_label)

decision_label_list = np.array(decision_label_list)
prev_decision_label_list = np.array(prev_decision_label_list)

for label in range(classes):
    print(np.count_nonzero(prev_decision_label_list == label))
    print(np.count_nonzero(decision_label_list == label))

confusion_matrix = np.zeros((4, 4), dtype=float)

for decision_label in range(classes):

    for true_label in range(classes):

        count = np.count_nonzero((label_list == true_label) & (
            decision_label_list == decision_label))

        confusion_matrix[decision_label][true_label] = count / \
            np.count_nonzero(label_list == true_label)

print(confusion_matrix)

plot_x_list = x_list[:, :2]
for label, marker in enumerate(['*', 'o', '^', 's']):

    label_plot_x_list = plot_x_list[label_list == label]
    correct_mask = (decision_label_list[label_list == label] == label)
    correct_plot_x_list = label_plot_x_list[correct_mask]
    wrong_plot_x_list = label_plot_x_list[~correct_mask]

    plt.scatter(
        correct_plot_x_list[:, 0], correct_plot_x_list[:, 1], marker=marker, c='green')
    plt.scatter(wrong_plot_x_list[:, 0],
                wrong_plot_x_list[:, 1], marker=marker, c='red')

plt.show()
