import numpy as np
import matplotlib.pyplot as plt
from q1_parameters import *
import util

x_list = np.loadtxt('q1_x_list.csv')
label_list = np.loadtxt('q1_label_list.csv', dtype=int)


def likelihood_ratio(x):

    return util.pdf(x, mean_1, np.diag(np.diag(covariance_1))) / util.pdf(x, mean_0, np.diag(np.diag(covariance_0)))


true_positive_rate_list = []
false_positive_rate_list = []

threshold_list = np.append(np.linspace(0, 10, num=101), np.inf)

min_probability_error = np.inf
best_point = None
best_threshold = None

for threshold in threshold_list:

    true_positive = 0
    false_negative = 0
    false_positive = 0
    true_negative = 0

    for x, true_label in zip(x_list, label_list):

        if likelihood_ratio(x) > threshold:
            decision_label = 1
        else:
            decision_label = 0

        if true_label == 1:
            if decision_label == 1:
                true_positive += 1
            else:
                false_negative += 1

        else:
            if decision_label == 1:
                false_positive += 1
            else:
                true_negative += 1

    true_positive_rate = util.get_true_positive_rate(
        true_positive, false_negative)
    false_positive_rate = util.get_false_positive_rate(
        false_positive, true_negative)

    print(threshold)

    true_positive_rate_list.append(true_positive_rate)
    false_positive_rate_list.append(false_positive_rate)

    probability_error = util.get_probability_error(
        true_positive, false_negative, prior_1, false_positive, true_negative, prior_0)

    if probability_error < min_probability_error:
        min_probability_error = probability_error
        best_point = [false_positive_rate, true_positive_rate]
        best_threshold = threshold

print('---')

true_positive_rate_list = np.array(true_positive_rate_list)
false_positive_rate_list = np.array(false_positive_rate_list)

plt.plot(false_positive_rate_list, true_positive_rate_list)
plt.scatter(best_point[0], best_point[1], s=100, marker='*', c='red')

plt.xlabel("false positive rate")
plt.ylabel("true positive rate")

plt.show()

print(min_probability_error)
print(best_threshold)
