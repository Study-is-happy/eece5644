import numpy as np
import matplotlib.pyplot as plt
from q1_parameters import *

x_list = []
label_list = []

for count in range(10000):

    if np.random.random() < prior_0:

        x = np.random.multivariate_normal(mean_0, covariance_0)
        label = 0

    else:

        x = np.random.multivariate_normal(mean_1, covariance_1)
        label = 1

    x_list.append(x)
    label_list.append(label)

x_list = np.array(x_list)
label_list = np.array(label_list)

np.savetxt('q1_x_list.csv', x_list)
np.savetxt('q1_label_list.csv', label_list, fmt='%i')

print(len(label_list[label_list == 0]))

plt.hist(x_list[:, 0][label_list == 0], 100)
plt.show()
