import numpy as np
import matplotlib.pyplot as plt
from q1_parameters import *
import util

x_list = np.loadtxt('q1_x_list.csv')
label_list = np.loadtxt('q1_label_list.csv', dtype=int)


def lda(x_list, label_list):

    x_0 = x_list[label_list == 0]
    x_1 = x_list[label_list == 1]

    mean_0 = np.mean(x_0, axis=0)
    mean_1 = np.mean(x_1, axis=0)

    covariance_0 = np.cov(x_0.T)
    covariance_1 = np.cov(x_1.T)

    scatter_between = (mean_0-mean_1).dot((mean_0-mean_1).T)
    scatter_within = covariance_0+covariance_1

    eigen_value_list, eigen_vector_list = np.linalg.eig(
        np.linalg.inv(scatter_within)*scatter_between)

    largest_index = np.argsort(eigen_value_list)[-1]

    return eigen_vector_list[largest_index]


weight_vector = lda(x_list, label_list)

proj_x_list = x_list.dot(weight_vector)

true_positive_rate_list = []
false_positive_rate_list = []

min_probability_error = np.inf
best_point = None
best_threshold = None

print(np.min(proj_x_list))
print(np.max(proj_x_list))

for threshold in np.linspace(np.min(proj_x_list), np.max(proj_x_list), num=100):

    true_positive = 0
    false_negative = 0
    false_positive = 0
    true_negative = 0

    for proj_x, true_label in zip(proj_x_list, label_list):

        if proj_x > threshold:
            decision_label = 1
        else:
            decision_label = 0

        if true_label == 1:
            if decision_label == 1:
                true_positive += 1
            else:
                false_negative += 1

        else:
            if decision_label == 1:
                false_positive += 1
            else:
                true_negative += 1

    true_positive_rate = util.get_true_positive_rate(
        true_positive, false_negative)
    false_positive_rate = util.get_false_positive_rate(
        false_positive, true_negative)

    print(threshold)

    true_positive_rate_list.append(true_positive_rate)
    false_positive_rate_list.append(false_positive_rate)

    probability_error = util.get_probability_error(
        true_positive, false_negative, prior_1, false_positive, true_negative, prior_0)

    if probability_error < min_probability_error:
        min_probability_error = probability_error
        best_point = [false_positive_rate, true_positive_rate]
        best_threshold = threshold

true_positive_rate_list = np.array(true_positive_rate_list)
false_positive_rate_list = np.array(false_positive_rate_list)

plt.plot(false_positive_rate_list, true_positive_rate_list)
plt.scatter(best_point[0], best_point[1], s=100, marker='*', c='red')

plt.xlabel("false positive rate")
plt.ylabel("true positive rate")

plt.show()

print(min_probability_error)
print(best_threshold)
