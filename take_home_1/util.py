import numpy as np


def pdf(x, mean, covariance):

    return 1/np.sqrt(np.power(2*np.pi, len(x))*np.linalg.det(covariance)) * \
        np.exp(-0.5*(x-mean).T.dot(np.linalg.inv(covariance)).dot(x-mean))


def get_probability_error(true_positive, false_negative, prior_1, false_positive, true_negative, prior_0):
    return false_negative/(true_positive+false_negative)*prior_1 + false_positive/(false_positive+true_negative)*prior_0


def get_true_positive_rate(true_positive, false_negative):
    return true_positive/(true_positive+false_negative)


def get_false_positive_rate(false_positive, true_negative):
    return false_positive/(false_positive+true_negative)
