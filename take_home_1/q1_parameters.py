import numpy as np

dimension = 2

prior_0 = 0.7

mean_0 = np.array([-1, -1, -1, -1])

covariance_0 = np.array([[2, -0.5, 0.3, 0],
                         [-0.5, 1, -0.5, 0],
                         [0.3, -0.5, 1, 0],
                         [0, 0, 0, 2]])

prior_1 = 0.3

mean_1 = np.array([1, 1, 1, 1])

covariance_1 = np.array([[1, 0.3, -0.2, 0],
                         [0.3, 2, 0.3, 0],
                         [-0.2, 0.3, 1, 0],
                         [0, 0, 0, 3]])
