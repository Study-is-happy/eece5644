import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from q2_parameters import *

x_list = []
label_list = []

cum_prior_list = np.cumsum(prior_list)

for count in range(10000):

    random_num = np.random.random()

    for index, cum_prior in enumerate(cum_prior_list):
        if random_num < cum_prior:
            x = np.random.multivariate_normal(
                mean_list[index], covariance_list[index])
            label = index
            x_list.append(x)
            label_list.append(label)
            break

x_list = np.array(x_list)
label_list = np.array(label_list)

np.savetxt('q2_x_list.csv', x_list)
np.savetxt('q2_label_list.csv', label_list, fmt='%i')

fig = plt.figure()
ax = Axes3D(fig)

for label in range(classes):
    x_label_list = x_list[label_list == label]
    ax.scatter(x_label_list[:, 0], x_label_list[:, 1], x_label_list[:, 2])

plt.show()
