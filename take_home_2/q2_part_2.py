import numpy as np
import matplotlib.pyplot as plt
import sklearn.mixture
import sklearn.model_selection

from q2_parameters import *
import util

x_list = np.loadtxt('q2_x_list_100.csv')

best_log_likelihood_list = []

for est_component_num in range(1, 21):

    print('M = '+str(est_component_num))

    parameter_num = est_component_num * 6 - 1

    gaussian_mixture_model = sklearn.mixture.GaussianMixture(
        n_components=est_component_num, max_iter=1000, n_init=2)

    k_fold = sklearn.model_selection.KFold(shuffle=True)

    log_likelihood_list = []

    for train_index, val_index in k_fold.split(x_list):

        gaussian_mixture_model.fit(x_list[train_index])
        # print(gaussian_mixture_model.score_samples(x_list[val_index]))
        log_likelihood = gaussian_mixture_model.score(x_list[val_index])

        log_likelihood_list.append(log_likelihood)

    mean_log_likelihood = np.mean(log_likelihood_list)
    print(mean_log_likelihood)
    best_log_likelihood_list.append(mean_log_likelihood)


print(np.argmax(best_log_likelihood_list)+1)
plt.plot(best_log_likelihood_list)
plt.xlabel("M")
plt.ylabel("log likelihood")
plt.show()
