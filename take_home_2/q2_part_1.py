import numpy as np
import matplotlib.pyplot as plt
import sklearn.mixture

from q2_parameters import *
import util

x_list = np.loadtxt('q2_x_list_100.csv')

bic_list = []

for est_component_num in range(1, 21):

    print('M = '+str(est_component_num))

    parameter_num = est_component_num * 6 - 1

    gaussian_mixture_model = sklearn.mixture.GaussianMixture(
        n_components=est_component_num, max_iter=1000, n_init=2)

    gaussian_mixture_model.fit(x_list)

    bic = np.log(len(x_list)) * parameter_num - 2 * \
        np.sum(gaussian_mixture_model.score_samples(x_list))

    print(bic)
    bic_list.append(bic)

print(np.argmin(bic_list)+1)
plt.plot(bic_list)
plt.xlabel("M")
plt.ylabel("BIC")
plt.show()
