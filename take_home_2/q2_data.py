import numpy as np
import matplotlib.pyplot as plt

from q2_parameters import *

x_list = []
# label_list = []

cum_weight_list = np.cumsum(weight_list)

for _ in range(100):

    random_num = np.random.random()

    for index, cum_weight in enumerate(cum_weight_list):

        if random_num < cum_weight:
            x = np.random.multivariate_normal(
                mean_list[index], covariance_list[index])
            x_list.append(x)
            # label_list.append(index)
            break


x_list = np.array(x_list)
# label_list = np.array(label_list)

np.savetxt('q2_x_list_100.csv', x_list)
# np.savetxt('q2_label_list.csv', label_list, fmt='%i')

plt.scatter(x_list[:, 0], x_list[:, 1])
plt.show()
