import numpy as np
import matplotlib.pyplot as plt

from q1_parameters import *

x_list = []
label_list = []

for _ in range(20000):

    if np.random.random() < prior_0:

        if np.random.random() < weight_0_0:
            x = np.random.multivariate_normal(mean_0_0, covariance_0_0)
        else:
            x = np.random.multivariate_normal(mean_0_1, covariance_0_1)
        label = 0

    else:
        x = np.random.multivariate_normal(mean_1, covariance_1)
        label = 1

    x_list.append(x)
    label_list.append(label)

x_list = np.array(x_list)
label_list = np.array(label_list)

np.savetxt('q1_x_list_val_20000.csv', x_list)
np.savetxt('q1_label_list_val_20000.csv', label_list, fmt='%i')

x_list_0 = x_list[label_list == 0]
plt.scatter(x_list_0[:, 0], x_list_0[:, 1], c='red')
x_list_1 = x_list[label_list == 1]
plt.scatter(x_list_1[:, 0], x_list_1[:, 1], c='blue')
plt.show()
