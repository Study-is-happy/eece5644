import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal

from q1_parameters import *
import util

x_list = np.loadtxt('q1_x_list_val_20000.csv')
label_list = np.loadtxt('q1_label_list_val_20000.csv', dtype=int)

true_positive_rate_list = []
false_positive_rate_list = []

threshold_list = np.append(np.linspace(0, 10, num=101), np.inf)

# theoretical_best_threshold = prior_0/prior_1
# threshold_list = util.insert_sorted_list(
#     threshold_list, theoretical_best_threshold)

val_best_threshold = np.count_nonzero(
    label_list == 0)/np.count_nonzero(label_list == 1)

print(np.count_nonzero(
    label_list == 0))
print(np.count_nonzero(
    label_list == 1))
threshold_list = util.insert_sorted_list(threshold_list, val_best_threshold)

min_probability_error = np.inf
best_point = None
best_threshold = None
best_decision_label_list = None

for threshold in threshold_list:

    likelihood_ratio_list = multivariate_normal.pdf(x_list, mean_1, covariance_1) / \
        (weight_0_0 * multivariate_normal.pdf(x_list, mean_0_0, covariance_0_0) +
         weight_0_1 * multivariate_normal.pdf(x_list, mean_0_1, covariance_0_1))

    true_positive = 0
    false_negative = 0
    false_positive = 0
    true_negative = 0

    decision_label_list = []

    for likelihood_ratio, true_label in zip(likelihood_ratio_list, label_list):

        if likelihood_ratio > threshold:
            decision_label = 1
        else:
            decision_label = 0

        decision_label_list.append(decision_label)

        if true_label == 1:
            if decision_label == 1:
                true_positive += 1
            else:
                false_negative += 1

        else:
            if decision_label == 1:
                false_positive += 1
            else:
                true_negative += 1

    true_positive_rate = true_positive/(true_positive+false_negative)
    false_positive_rate = false_positive/(false_positive+true_negative)

    print(threshold)

    true_positive_rate_list.append(true_positive_rate)
    false_positive_rate_list.append(false_positive_rate)

    probability_error = false_negative / (true_positive+false_negative) * prior_1 + \
        false_positive / (false_positive+true_negative) * prior_0

    # if probability_error < min_probability_error:
    if threshold == val_best_threshold:
        min_probability_error = probability_error
        best_point = [false_positive_rate, true_positive_rate]
        best_threshold = threshold
        best_decision_label_list = np.array(decision_label_list)

print('---')

print(best_threshold)
print(min_probability_error)

true_positive_rate_list = np.array(true_positive_rate_list)
false_positive_rate_list = np.array(false_positive_rate_list)

plt.plot(false_positive_rate_list, true_positive_rate_list)
plt.scatter(best_point[0], best_point[1], s=100, marker='*', c='red')

plt.xlabel("false positive rate")
plt.ylabel("true positive rate")

plt.show()

x_list_0 = x_list[best_decision_label_list == 0]
plt.scatter(x_list_0[:, 0], x_list_0[:, 1], c='red')
x_list_1 = x_list[best_decision_label_list == 1]
plt.scatter(x_list_1[:, 0], x_list_1[:, 1], c='blue')
plt.show()
