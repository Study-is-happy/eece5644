import numpy as np

dimension = 2

prior_0 = 0.6

weight_0_0 = 0.5

mean_0_0 = np.array([5, 0])

covariance_0_0 = np.array([[4, 0],
                           [0, 2]])

weight_0_1 = 0.5

mean_0_1 = np.array([0, 4])

covariance_0_1 = np.array([[1, 0],
                           [0, 3]])

prior_1 = 0.4

mean_1 = np.array([3, 2])

covariance_1 = np.array([[2, 0],
                         [0, 2]])
