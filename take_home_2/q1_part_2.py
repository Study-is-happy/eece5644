import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal

from q1_parameters import *
import util

x_list_train = np.loadtxt('q1_x_list_train_100.csv')
label_list_train = np.loadtxt('q1_label_list_train_100.csv', dtype=int)

x_list_0 = x_list_train[label_list_train == 0]

est_label_num = 2
est_weight_list = np.ones(est_label_num) / est_label_num
est_mean_list = x_list_0[np.random.choice(len(x_list_0), est_label_num)]

distance_list = []
for index in range(est_label_num):
    distance_list.append(np.linalg.norm(x_list_0-est_mean_list[index], axis=1))

est_label_list = np.argmin(distance_list, axis=0)

est_covariance_list = []
for index in range(est_label_num):
    est_mean_diff = x_list_0[est_label_list == index]-est_mean_list[index]
    est_covariance_list.append(
        est_mean_diff.T.dot(est_mean_diff)/(len(x_list_0)-1))

est_covariance_list = np.array(est_covariance_list)

converge_diff_list = []

while True:

    prev_est_weight_list = est_weight_list
    prev_est_mean_list = est_mean_list
    prev_est_covariance_list = est_covariance_list

    expectation_list = []
    for index in range(est_label_num):
        expectation_list.append(est_weight_list[index]*multivariate_normal.pdf(
            x_list_0, est_mean_list[index], est_covariance_list[index]))

    expectation_list /= np.sum(expectation_list, axis=0)

    sum_expectation_list = np.sum(expectation_list, axis=1)

    est_weight_list = sum_expectation_list/len(x_list_0)

    est_mean_list = expectation_list.dot(
        x_list_0)/np.tile(sum_expectation_list, (dimension, 1)).T

    est_covariance_list = []
    for index in range(est_label_num):

        est_mean_diff = x_list_0 - est_mean_list[index]
        est_covariance_list.append(
            (np.tile(expectation_list[index], (dimension, 1))*est_mean_diff.T).dot(est_mean_diff)/sum_expectation_list[index])

    est_covariance_list = np.array(est_covariance_list)

    converge_diff = np.sum(np.abs(est_weight_list-prev_est_weight_list)) + \
        np.sum(np.abs(est_mean_list-prev_est_mean_list)) + \
        np.sum(np.abs(est_covariance_list-prev_est_covariance_list))

    print(converge_diff)

    converge_diff_list.append(converge_diff)

    if converge_diff < 0.01:
        break

print('---')

print(est_weight_list)
print(est_mean_list)
print(est_covariance_list)

plt.plot(converge_diff_list)
plt.show()

x_list_1 = x_list_train[label_list_train == 1]
est_mean = np.mean(x_list_1, axis=0)
est_mean_diff = x_list_1-est_mean
est_covariance = est_mean_diff.T.dot(est_mean_diff)/(len(x_list_1)-1)

print(est_mean)
print(est_covariance)

est_prior_1 = np.count_nonzero(label_list_train == 1)/len(label_list_train)
est_prior_0 = np.count_nonzero(label_list_train == 0)/len(label_list_train)

x_list_val = np.loadtxt('q1_x_list_val_20000.csv')
label_list_val = np.loadtxt('q1_label_list_val_20000.csv', dtype=int)

true_positive_rate_list = []
false_positive_rate_list = []

threshold_list = np.append(np.linspace(0, 10, num=101), np.inf)

val_best_threshold = np.count_nonzero(
    label_list_val == 0)/np.count_nonzero(label_list_val == 1)

threshold_list = util.insert_sorted_list(threshold_list, val_best_threshold)

min_probability_error = np.inf
best_point = None
best_threshold = None
best_decision_label_list = None

for threshold in threshold_list:

    likelihood_ratio_list = multivariate_normal.pdf(x_list_val, est_mean, est_covariance) / \
        (est_weight_list[0] * multivariate_normal.pdf(x_list_val, est_mean_list[0], est_covariance_list[0]) +
         est_weight_list[1] * multivariate_normal.pdf(x_list_val, est_mean_list[1], est_covariance_list[1]))

    true_positive = 0
    false_negative = 0
    false_positive = 0
    true_negative = 0

    decision_label_list = []

    for likelihood_ratio, true_label in zip(likelihood_ratio_list, label_list_val):

        if likelihood_ratio > threshold:
            decision_label = 1
        else:
            decision_label = 0

        decision_label_list.append(decision_label)

        if true_label == 1:
            if decision_label == 1:
                true_positive += 1
            else:
                false_negative += 1

        else:
            if decision_label == 1:
                false_positive += 1
            else:
                true_negative += 1

    true_positive_rate = true_positive/(true_positive+false_negative)
    false_positive_rate = false_positive/(false_positive+true_negative)

    # print(threshold)

    true_positive_rate_list.append(true_positive_rate)
    false_positive_rate_list.append(false_positive_rate)

    probability_error = false_negative / (true_positive+false_negative) * est_prior_1 + \
        false_positive / (false_positive+true_negative) * est_prior_0

    print(probability_error)

    # if probability_error < min_probability_error:
    if threshold == val_best_threshold:
        min_probability_error = probability_error
        best_point = [false_positive_rate, true_positive_rate]
        best_threshold = threshold
        best_decision_label_list = np.array(decision_label_list)

print('---')

print(best_threshold)
print(min_probability_error)

true_positive_rate_list = np.array(true_positive_rate_list)
false_positive_rate_list = np.array(false_positive_rate_list)

plt.plot(false_positive_rate_list, true_positive_rate_list)
plt.scatter(best_point[0], best_point[1], s=100, marker='*', c='red')

plt.xlabel("false positive rate")
plt.ylabel("true positive rate")

plt.show()
