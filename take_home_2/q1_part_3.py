import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

from q1_parameters import *
import util


def cost_func(weight, x_list, label_list):

    h = 1 / (1+np.exp(-weight.T.dot(x_list.T)))

    return (-1/len(label_list))*((np.sum(label_list * np.log(h))) + (np.sum((1-label_list) * np.log(1-h))))


def linear_func(x_list):
    return np.hstack((np.ones((len(x_list), 1)), x_list))


def quadratic_func(x_list):

    return np.hstack((
        np.ones((len(x_list), 1)),
        np.array([x_list[:, 0]]).T,
        np.array([x_list[:, 1]]).T,
        np.array([x_list[:, 0]**2]).T,
        np.array([x_list[:, 0] * x_list[:, 1]]).T,
        np.array([x_list[:, 1]**2]).T
    ))


x_list_train = np.loadtxt('q1_x_list_train_1000.csv')
label_list_train = np.loadtxt('q1_label_list_train_1000.csv', dtype=int)
x_list_val = np.loadtxt('q1_x_list_val_20000.csv')
label_list_val = np.loadtxt('q1_label_list_val_20000.csv', dtype=int)

x_list_train = quadratic_func(x_list_train)
# weight = np.ones(dimension+1)
weight = np.zeros(6)

weight = scipy.optimize.fmin(
    func=cost_func, x0=weight, args=(x_list_train, label_list_train))

x_list_val = quadratic_func(x_list_val)
y_list = -weight.dot(x_list_val.T)

val_best_threshold = np.log(np.count_nonzero(
    label_list_val == 0)/np.count_nonzero(label_list_val == 1))

decision_label_list = y_list < val_best_threshold

x_list_0 = x_list_val[decision_label_list == 0]
plt.scatter(x_list_0[:, 1], x_list_0[:, 2], c='red')
x_list_1 = x_list_val[decision_label_list == 1]
plt.scatter(x_list_1[:, 1], x_list_1[:, 2], c='blue')
plt.show()

est_prior_1 = np.count_nonzero(label_list_train == 1)/len(label_list_train)
est_prior_0 = np.count_nonzero(label_list_train == 0)/len(label_list_train)

threshold_list = np.linspace(np.min(y_list), np.max(y_list), num=100)

threshold_list = util.insert_sorted_list(threshold_list, val_best_threshold)

true_positive_rate_list = []
false_positive_rate_list = []

min_probability_error = np.inf
best_point = None
best_threshold = None
best_decision_label_list = None

for threshold in threshold_list:

    true_positive = 0
    false_negative = 0
    false_positive = 0
    true_negative = 0

    decision_label_list = []

    for y, true_label in zip(y_list, label_list_val):

        if y < threshold:
            decision_label = 1
        else:
            decision_label = 0

        decision_label_list.append(decision_label)

        if true_label == 1:
            if decision_label == 1:
                true_positive += 1
            else:
                false_negative += 1

        else:
            if decision_label == 1:
                false_positive += 1
            else:
                true_negative += 1

    true_positive_rate = true_positive/(true_positive+false_negative)
    false_positive_rate = false_positive/(false_positive+true_negative)

    print(threshold)

    true_positive_rate_list.append(true_positive_rate)
    false_positive_rate_list.append(false_positive_rate)

    probability_error = false_negative / (true_positive+false_negative) * est_prior_1 + \
        false_positive / (false_positive+true_negative) * est_prior_0

    if threshold == val_best_threshold:
        min_probability_error = probability_error
        best_point = [false_positive_rate, true_positive_rate]
        best_threshold = threshold
        best_decision_label_list = np.array(decision_label_list)

print('---')

print(best_threshold)
print(min_probability_error)

true_positive_rate_list = np.array(true_positive_rate_list)
false_positive_rate_list = np.array(false_positive_rate_list)

plt.plot(false_positive_rate_list, true_positive_rate_list)
plt.scatter(best_point[0], best_point[1], s=100, marker='*', c='red')

plt.xlabel("false positive rate")
plt.ylabel("true positive rate")

plt.show()
