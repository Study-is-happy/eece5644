import numpy as np


def insert_sorted_list(sorted_list, value):

    insert_index = np.searchsorted(
        sorted_list, value)
    sorted_list = np.insert(
        sorted_list, insert_index, value)

    return sorted_list
