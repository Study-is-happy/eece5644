import numpy as np

dimension = 2

component_num = 10

weight_list = np.ones(component_num) / component_num

mean_list = np.tile(np.linspace(10, 100, num=component_num), (dimension, 1)).T

variance_list = (np.linspace(1, 10, num=component_num)/2)**2
covariance_list = []
for variance in variance_list:
    covariance_list.append(np.diag([variance]*dimension))

covariance_list = np.array(covariance_list)
